# nutz-react-webpack

#### Description
react + Ant Design Mobile of React jsp 多页面支持

#### Software Architecture
Software architecture description

#### Installation

1. node.js环境 


#### Instructions

1. src/main/front 进入目录下 
2. npm install
3. npm run dev
4. 启动后台
5. localhost:8081/antdmobile

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)