package react;

import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import java.util.HashMap;
import java.util.Map;

public class MainModule {

    @At("/hello")
    @Ok("jsp:jsp.hello")
    public String doHello() {
        return "Hello Nutz";
    }

    @At("/amaze")
    @Ok("jsp:jsp.amaze.index")
    public String amaze() {
        return "Hello Nutz";
    }

    @At("/antdmobile")
    @Ok("jsp:jsp.antdmobile.index")
    public String antdmobile() {
        return "Hello Nutz";
    }

    @At("/login")
    @Ok("jsp:jsp.antdmobile.login")
    public String login() {
        return "Hello Nutz";
    }


    @At({"/", "index"})
    @Ok("jsp:jsp.index.index")
    public void index() throws Exception {
    }

    @At("/start")
    @Ok("jsp:jsp.start.index")
    public void start() throws Exception {
    }

    @At("/contact")
    @Ok("jsp:jsp.contact.index")
    public void contact() throws Exception {
    }

    @At("/404")
    @Ok("jsp:jsp.index.index")
    public String notFound() throws Exception {
        return "index/index";
    }

    @At("/testData")
    @Ok("json")
    public Object testData() {
       Map map = new HashMap<>();
       map.put("test", "sdfsd");
        return map;
    }

}
