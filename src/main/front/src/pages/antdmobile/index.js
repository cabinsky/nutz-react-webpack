import React from 'react'
import ReactDOM from 'react-dom'
import 'antd-mobile/dist/antd-mobile.css'
import ErrorInputExample from './ErrorInputExample'
import BasiceInputExample from './BasicInputExample'
import H5NumberInputExample from './H5NumberInputExample'
import MenuExample from './MenuExample'
import { createForm } from 'rc-form'
import { BrowserRouter, Route , NavLink} from 'react-router-dom'

const BasiceInputExampleWrapper = createForm()(BasiceInputExample);
const H5NumberInputExampleWrapper = createForm()(H5NumberInputExample);

const App = () => (
  <BrowserRouter >
    <div>
    <div>
      <MenuExample/>
    </div>
    <Route path="/antdmobile" exact component={BasiceInputExampleWrapper} />
    <Route path="/antdmobile/we" component={H5NumberInputExampleWrapper} />
    </div>
  </BrowserRouter>
)


//ReactDOM.render(<div><Button>Start</Button><BasiceInputExampleWrapper/><ErrorInputExample/><H5NumberInputExampleWrapper/></div>, document.getElementById('root'));

ReactDOM.render(<App/>, document.getElementById('root'))
