
import React from 'react'
import ReactDOM from 'react-dom'
import { WingBlank, WhiteSpace , List, InputItem, Button} from 'antd-mobile'
import { createForm } from 'rc-form'

import 'antd-mobile/dist/antd-mobile.css'
import './login.css'


class Login extends React.Component {
  componentDidMount() {
    // this.autoFocusInst.focus();
  }
  handleClick = () => {
    this.inputRef.focus();
  }
  render() {
    const { getFieldProps } = this.props.form;
    return (
  <div style={{ padding: '0px 0' }}>
    <WingBlank/>
    <WhiteSpace size="md"></WhiteSpace>
    <WingBlank size="sm">
      <List renderHeader={() => 'Login'} className={`placeholder`}>
      <InputItem
        {...getFieldProps('autofocus')}
        clear
        placeholder="auto focus"
        ref={el => this.autoFocusInst = el}
      >用户名</InputItem>
      <InputItem
        {...getFieldProps('focus')}
        clear
        placeholder="click the button below to focus"
        ref={el => this.inputRef = el}
      >密 码</InputItem>

      <List.Item>
        <Button type="primary">登录</Button>
        <WhiteSpace size="lg"></WhiteSpace>
        <Button type="primary">注册</Button>
      </List.Item>
      </List>
    </WingBlank>

  </div>
    );
  }
}
const LoginWrapper = createForm()(Login);

ReactDOM.render(<LoginWrapper/>, document.getElementById('root'))
