/* eslint global-require:0, no-nested-ternary:0 */
import React from 'react';
import { Menu, ActivityIndicator, NavBar ,TabBar, List, } from 'antd-mobile';
import { BrowserRouter, Route , NavLink, Router, Switch} from 'react-router-dom'

const data = [
  {
    value: '1',
    label: 121,
  }, {
    value: '2',
    label: 'Supermarket',
  },
  {
    value: '3',
    label: 'Extra',
    isLeaf: true,
  },
];

class MenuExample extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {
      initData: '',
      show: false,
    };
  }
  onChange = (value) => {
    console.log(value)
    let label = '';
    data.forEach((dataItem) => {
      if (dataItem.value === value[0]) {
        label = dataItem.label;
        if (dataItem.children && value[1]) {
          dataItem.children.forEach((cItem) => {
            if (cItem.value === value[1]) {
              label += ` ${cItem.label}`;
            }
          });
        }
      }
    });
    console.log(label);
  }
  handleClick = (e) => {
    //e.preventDefault(); // Fix event propagation on Android
    this.setState({
      show: !this.state.show,
    });
    // mock for async data loading
    if (!this.state.initData) {
      setTimeout(() => {
        this.setState({
          initData: data,
        });
      }, 1000);
    }
  }

  onMaskClick = () => {
    this.setState({
      show: false,
    });
  }
  onClick = (key,event) => {
    console.log(this.props.history)
    this.props.history.push('/antdmobile/'+key);
  }

  render() {
    const { initData, show } = this.state;
    const Item = List.Item;
    const menuEl = (
    /*{ <Menu
       className="single-foo-menu"
       data={initData}
       value={['2']}
       level={1}
       onChange={this.onChange}
       height={document.documentElement.clientHeight * 0.6}
     >}*/
      <List>
        <Item>
          <NavLink exact activeClassName="selected" onClick={this.handleClick} to="/antdmobile">index</NavLink>
        </Item>
        <Item>
          <NavLink exact activeClassName="selected" onClick={this.handleClick} to="/antdmobile/we">index</NavLink>
        </Item>

      {/*<Item arrow="horizontal" onClick={this.onClick.bind(this,'accordion')}>Accordion 手风琴</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'we')}>ActionSheet 动作面板</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'card')}>Card 卡片</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'drawer')}>Drawer 抽屉</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'list')}>List 列表</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'listView')}>ListView 长列表</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'modal')}>Modal 对话框</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'menu')}>Menu 菜单</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'popup')}>Popup 弹出层</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'popover')}>Popover 气泡</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'result')}>Result 结果页</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'toast')}>Toast 轻提示</Item>
      <Item arrow="horizontal" onClick={this.onClick.bind(this,'table')}>Table 表格</Item>*/}
    </List>

    );
    const loadingEl = (
      <div style={{ position: 'absolute', width: '100%', height: document.documentElement.clientHeight * 0.6, display: 'flex', justifyContent: 'center' }}>
        <ActivityIndicator size="large" />
      </div>
    );
    return (
      <div className={show ? 'single-menu-active' : ''}>
        <div>
          <NavBar
            leftContent="Menu"
            mode="light"
            onLeftClick={this.handleClick}
            className="single-top-nav-bar"
          >
            OneLevel menu
          </NavBar>
        </div>
        {show ? initData ? menuEl : loadingEl : null}
        {show ? <div className="menu-mask" onClick={this.onMaskClick} /> : null}
      </div>
    );
  }
}

export default MenuExample
