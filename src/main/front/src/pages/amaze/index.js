import React from 'react';
import ReactDOM from 'react-dom';
import 'amazeui-touch/dist/amazeui.touch.min.css';
import {
  Button,
} from 'amazeui-touch';
import ButtonExample from "./ButtonExample"

ReactDOM.render(<div><Button hollow amStyle="secondary">Secondary</Button>npm
  <Button hollow amStyle="success">Success</Button>
  <Button hollow amStyle="warning">Warning</Button>
  <Button hollow amStyle="alert">Alert</Button>
  <Button hollow amStyle="dark">Dark</Button><ButtonExample></ButtonExample></div>, document.getElementById('root'));
