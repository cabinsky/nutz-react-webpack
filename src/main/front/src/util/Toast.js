import { Toast } from 'antd-mobile';

const showToast = (msg = 'This is a toast tips !!!') => {
  Toast.info(msg, 1);
}

const showToastNoMask = (msg = 'Toast without mask !!!') => {
  Toast.info(msg, 2, null, false);
}

const successToast = (msg = 'Load success !!!') => {
  Toast.success(msg, 1);
}

const failToast = (msg = 'Load failed !!!') => {
  Toast.fail(msg, 1);
}

const offline = (msg = 'Network connection failed !!!') => {
  Toast.offline(msg, 1);
}

const loadingToast = (msg = 'Loading...') => {
  Toast.loading(msg, 1, () => {
    //console.log('Load complete !!!');
  });
}

const closeToast = () => {
  Toast.hide();
}

export default {
  showToast,
  showToastNoMask,
  successToast,
  failToast,
  offline,
  loadingToast,
  closeToast
}

