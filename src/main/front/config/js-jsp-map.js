'use strict'
const utils = require('../build/utils')

const pathResolve = utils.pathResolve

function jsPathResolve(path) {
  return pathResolve.src('pages', path)
}

function jspPathResolve(path) {
  return pathResolve.dist('WEB-INF/jsp', path)
}

const jsJspMap = [
  { name: 'index', jsPath: jsPathResolve('index/index.js'), jspPath: jspPathResolve('index/index.jsp') },
  { name: 'start', jsPath: jsPathResolve('start/index.js'), jspPath: jspPathResolve('start/index.jsp') },
  { name: 'contact', jsPath: jsPathResolve('contact/index.js'), jspPath: jspPathResolve('contact/index.jsp') },
  { name: 'amaze', jsPath: jsPathResolve('amaze/index.js'), jspPath: jspPathResolve('amaze/index.jsp') },
  { name: 'antdmobile', jsPath: jsPathResolve('antdmobile/index.js'), jspPath: jspPathResolve('antdmobile/index.jsp') },
  { name: 'login', jsPath: jsPathResolve('antdmobile/login.js'), jspPath: jspPathResolve('antdmobile/login.jsp') }
]

module.exports = jsJspMap
